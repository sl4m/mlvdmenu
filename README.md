# mlvdmenu

A dmenu shell script to connect to [Mullvad's](https://mullvad.net) vpn servers.

## Vpn Connection Types

1. WireGuard **FASTEST**
2. OpenVpn
3. OpenVpn + ShadowSocks (to circumvent restriction/censorship). **SLOWEST**


## Prerequisites

- [password patched](https://tools.suckless.org/dmenu/patches/password/) dmenu

    > NOTE: If you do not care about your password being hidden remove the -P flag in .config/MullvadVpn/askpass.sh or in the script before installing

- jq
- notify-send ( recommended, uses echo if not found)
- openresolv for DNS functionality
- A WireGuard private key and CIDR Address obtained from a WireGuard config file (ex: mlvd-de15.conf) [generated here](https://mullvad.net/en/account/#/wireguard-config/)
- Your Mullvad account ID and your OpenVpn certificate that you can get by generating a openvpn config [here](https://mullvad.net/en/account/#/openvpn-config/)
- At least one of these packages:
    - wireguard-tools for the Wireguard connection
    - openvpn for the OpenVpn connection
    - openvpn + shadowsocks-libev for connecting with Openvpn through a shadowsocks bridge

## Installation

    $ git clone https://codeberg.org/sl4m/mlvdmenu && cd "$(basename $_)"
    $ chmod +x ./mlvdmenu.sh && ./mlvdmenu.sh

## TODO

- [X] Choose random IP when user doesn't specify
- [X] Filter Servers that are down

