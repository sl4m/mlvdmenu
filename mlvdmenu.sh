#!/bin/sh

MLVD_CFGDIR="$HOME/.config/mlvdmenu"
WG_DIR="$MLVD_CFGDIR/wg"
OVPN_DIR="$MLVD_CFGDIR/ovpn/"
SERVERS="$MLVD_CFGDIR/servers.json"

# askpass.sh is sudo's ASKPASS script see sudo(8).
export SUDO_ASKPASS="$MLVD_CFGDIR/askpass.sh"

# Print with Colors.
alias RED='tput setaf 1'
alias GREEN='tput setaf 2'
alias YELLOW='tput setaf 3'
alias NOCLR='tput sgr 0'


check_deps() {
    for prog in dmenu jq; do
        [ ! "$(which "$prog")" ] && echo "$(RED)Please install $prog!$(NOCLR)" && exit 1
    done

    [ ! "$(which resolvconf)" ] && "$(RED)Please install openresolv!$(NOCLR)" && exit 1

    if [ ! $(which ss-local) && ! $(which openvpn) ]  ||  [ ! $(which wg-quick) ] || [ ! $(which openvpn) ]; then
        echo "$(RED)Please Install either wireguard-tools OR openvpn OR openvpn + shadowsocks-libev$(NOCLR)" && exit 1
    fi
}

notify() {
    if [ ! "$(which notify-send 2>/dev/null)" ] ;then
        echo "$(RED)[!] notify-send Not found please install libnotify!!$(NOCLR) Using echo meanwhile" && echo "$@"
    else
        notify-send -i "$MLVD_CFGDIR/mullvad_icon.png" "[ VPN]" "$@"
    fi
}

connect() {
    sudo -Av
    connection_type=$( (echo wg; echo shadowsocks+openvpn;echo openvpn )|dmenu -p '[ VPN] Choose protocol')
    case $connection_type in

        shadowsocks+openvpn)
            bridge=$(jq -r 'select(.type == "bridge")|.hostname+"/"+.ipv4_addr_in' $SERVERS  | dmenu -l 5 -p '[ VPN] Choose ShadowSocks bridge' | awk -F/ '{print $2}')
            ovpn_host=$(jq -r 'select(.type == "openvpn") | .hostname+"/"+.ipv4_addr_in' $SERVERS |dmenu -l 5 -p "[ VPN] Choose OpenVpn location" | awk -F/ '{print $2}')
            sed -ie "s/remote .*$/remote $ovpn_host 443/g" $OVPN_DIR/mullvad_ovpnssocks.conf
            ss-local -s $bridge -p 443 -l 1080 -k '23#dfsbbb' -m chacha20 &
            cd $OVPN_DIR && sudo openvpn --config mullvad_ovpnssocks.conf &
            notify "Connecting (Shadowsocks+Ovpn) to $ovpn_host"
            ;;
        openvpn)
            ovpn_host=$(jq -r 'select(.type == "openvpn") | .hostname+"/"+.ipv4_addr_in' $SERVERS |dmenu -l 5 -p "[ VPN] Choose OpenVpn location" | awk -F/ '{print $2}')
            if [ -z "$ovpn_host" ];then
                ovpn_host=$(jq -r 'select(.type == "openvpn") | .ipv4_addr_in' $SERVERS | shuf -n1)
            fi
            sed -ie "s/remote .*$/remote $ovpn_host 1300/g" $OVPN_DIR/mullvad_ovpn.conf
            cd $OVPN_DIR && sudo openvpn --config mullvad_ovpn.conf &
            notify "Connecting (Ovpn) to $ovpn_host"
            ;;
        wg)
            wg_host=$(jq -r 'select(.type == "wireguard") | .hostname+"/"+.ipv4_addr_in' $SERVERS |dmenu -l 5 -p "[ VPN] Choose WireGuard location" | awk -F/ '{print $2}')
            if [ -z "$wg_host" ];then
                wg_host=$(jq -r 'select(.type == "wireguard") | .ipv4_addr_in' $SERVERS | shuf -n1 )
            fi
            cat $SERVERS | jq -r "select(.ipv4_addr_in == \"$wg_host\") | \"[Interface]\nPrivateKey = $(head -1 $WG_DIR/creds.txt)\nAddress = $(tail -1 $WG_DIR/creds.txt)\nDNS = 193.138.218.74\n\n[Peer]\nPublicKey = \"+.pubkey+\"\nAllowedIps = 0.0.0.0/0\nEndPoint = \"+.ipv4_addr_in+\":51820\"" > $WG_DIR/mullvad.conf && chmod 600 $WG_DIR/mullvad.conf
            sudo wg-quick up $WG_DIR/mullvad.conf &
            notify "Connecting (Wireguard) to $wg_host"
            ;;
    esac

    # Output the Host we are currently connected to /tmp/isvpnon (To use in a statusbar for ex).
    curl --retry-delay 1 --retry 9001 -fSsL https://am.i.mullvad.net/json 2>/dev/null |jq -r '.mullvad_exit_ip_hostname' > /tmp/isvpnon
    if [ "$(curl -fsSl https://am.i.mullvad.net/json 2>/dev/null |jq .blacklisted.blacklisted)" = "true" ]; then
        notify "Ip Blacklisted!";
    fi
    exit 0
}

# Checks if the config directory exists if not it creates it and installs.
if [ ! -d $MLVD_CFGDIR ]; then
    check_deps 2>/dev/null
    echo "$(GREEN)[*] Installing mlvdmenu...$(NOCLR)"
    mkdir -p $MLVD_CFGDIR/wg && mkdir -p $MLVD_CFGDIR/ovpn
    curl -fsSL https://api.mullvad.net/www/relays/all/ |jq -r '.[]' > "$SERVERS"
    mv ./mullvad_icon.png "$MLVD_CFGDIR/mullvad_icon.png"
    mv ./mullvad_ovpn_ssocks.conf "$OVPN_DIR/mullvad_ovpnssocks.conf"
    mv ./mullvad_ovpn.conf "$OVPN_DIR/mullvad_ovpn.conf"
    sudo mv ./update-resolv-conf /etc/openvpn/update-resolv-conf && sudo chmod +x /etc/openvpn/update-resolv-conf
    echo "[?] Enter your Mullvad Account ID : " && read acc_id && echo "$acc_id\nm" > "$OVPN_DIR/mullvad_userpass.txt" && chmod 600 "$OVPN_DIR/mullvad_userpass.txt"

    echo "[?] Enter your Mullvad Public Certificate (mullvad_ca.crt): $(YELLOW) Press ctrl-D when done$(NOCLR)" && IFS= cert=$(cat) && echo $cert> "$OVPN_DIR/mullvad_ca.crt" && chmod 600 "$OVPN_DIR/mullvad_ca.crt"
    echo '#!/bin/sh\necho $(dmenu -P -p "$1"  <&- )' > "$MLVD_CFGDIR/askpass.sh" && chmod +x "$MLVD_CFGDIR/askpass.sh"
    echo "[?] Enter wg private key: " && read wg_key && echo "$wg_key" > "$WG_DIR/creds.txt"
    echo "[?] Enter wg Address: " && read addr && echo "$addr" >> "$WG_DIR/creds.txt"
    chmod 600 "$WG_DIR/creds.txt"
    echo "$(GREEN)[*] Finished.$(NOCLR)"
    exit 0
else
    ison="$(ip a | grep -c 'tun0\|mullvad')"
    if [ $ison -eq 0 ]; then
        if [ $(($(date +%s)-$(stat --printf=%Y $SERVERS))) -gt 86400 ]; then
            notify "Updating Servers Please Wait.."
            curl -fsSL https://api.mullvad.net/www/relays/all/ | jq -r '.[] | select(.active==true)' > "$SERVERS" && killall notify-send
        fi
        connect
    else
        sudo wg-quick down $MLVD_CFGDIR/wg/mullvad.conf;
        sudo killall ss-local openvpn 2>/dev/null
        echo -n "Off" > /tmp/isvpnon
        notify "Disconnected"
        exit 0
    fi
fi
